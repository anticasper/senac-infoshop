
<?php
/* =============================================
    Nome: especificacaoDao.php
    Autor: Diovanni de Souza
    Data Criação: 11/03/2020
    Descrição: Dao das especificacaos
    Ultima Mudança: 11/03/2020
============================================= */
    include_once "classes/models/especificacao.php";
    include_once "includes/banco.php";
    class EspecificacaoDao{
        public function incluir($especificacao){
            $banco = new Banco();
            $query = "INSERT INTO 
                        especificacao 
                        (
                            codigo,
                            nome,
                            descricao,
                            codigo_produto
                        ) VALUES (
                            NULL,
                            '" . $especificacao->getNome() . "',
                            '" . $especificacao->getDescricao() . "',
                            '" . $especificacao->getCodigoProduto() . "'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();
            $query = "SELECT * FROM especificacao";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){
                    $especificacao = new especificacaoModel();
                    $especificacao->setCodigo($dados['codigo']);
                    $especificacao->setNome($dados['nome']);
                    $especificacao->setDescricao($dados['descricao']);
                    $especificacao->setCodigoProduto($dados['codigo_produto']);
                    array_push($lista, $especificacao);
                }
                return $lista;
            }
        }
        public function editar($especificacao){
            $banco = new Banco();
            $query = "  UPDATE 
                            especificacao 
                        SET
                            nome = '" . $especificacao->getNome() . "',
                            descricao = '" . $especificacao->getDescricao() . "',
                            codigo_produto = '" . $especificacao->getCodigoProduto() . "'
                        WHERE
                            codigo = " . $especificacao->getCodigo() . " ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM especificacao WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $especificacao = new especificacaoModel();
                $especificacao->setCodigo($dados['codigo']);
                $especificacao->setNome($dados['nome']);
                $especificacao->setDescricao($dados['descricao']);
                $especificacao->setCodigoProduto($dados['codigo_produto']);
                return $especificacao;
            }
            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM especificacao WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }

    }
