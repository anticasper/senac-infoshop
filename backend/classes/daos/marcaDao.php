
<?php
/* =============================================
    Nome: marcaDao.php
    Autor: Diovanni de Souza
    Data Criação: 09/03/2020
    Descrição: Dao das Marcas
    Ultima Mudança: 09/03/2020
============================================= */
    include_once "classes/models/marca.php";
    include_once "includes/banco.php";
    class MarcaDao{
        public function incluir($marca){
            $banco = new Banco();
            $query = "INSERT INTO 
                        marca 
                        (
                            codigo,
                            nome,
                            descricao,
                            logo
                        ) VALUES (
                            NULL,
                            '" . $marca->getNome() . "',
                            '" . $marca->getDescricao() . "',
                            '" . $marca->getLogo() . "'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();
            $query = "SELECT * FROM marca";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){
                    $marca = new MarcaModel();
                    $marca->setCodigo($dados['codigo']);
                    $marca->setNome($dados['nome']);
                    $marca->setDescricao($dados['descricao']);
                    $marca->setLogo($dados['logo']);
                    array_push($lista, $marca);
                }
                return $lista;
            }
        }
        public function editar($marca){
            $banco = new Banco();
            $query = "  UPDATE 
                            marca 
                        SET
                            nome = '" . $marca->getNome() . "',
                            descricao = '" . $marca->getDescricao() . "',
                            logo = '" . $marca->getLogo() . "'
                        WHERE
                            codigo = " . $marca->getCodigo() . " ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM marca WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $marca = new MarcaModel();
                $marca->setCodigo($dados['codigo']);
                $marca->setNome($dados['nome']);
                $marca->setDescricao($dados['descricao']);
                $marca->setLogo($dados['logo']);
                return $marca;
            }
            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM marca WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }

    }
