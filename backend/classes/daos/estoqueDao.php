
<?php
/* =============================================
    CodigoProduto: estoqueDao.php
    Autor: Diovanni de Souza
    Data Criação: 11/03/2020
    Descrição: Dao das Estoques
    Ultima Mudança: 11/03/2020
============================================= */
    include_once "classes/models/estoque.php";
    include_once "includes/banco.php";
    class EstoqueDao{
        public function incluir($estoque){
            $banco = new Banco();
            $query = "INSERT INTO 
                        estoque 
                        (
                            codigo,
                            codigo_produto,
                            maximo,
                            minimo,
                            quantidade_atual,
                            quantidade_reservada
                        ) VALUES (
                            NULL,
                            '" . $estoque->getCodigoProduto() . "',
                            '" . $estoque->getMaximo() . "',
                            '" . $estoque->getMinimo() . "',
                            '" . $estoque->getQuantidadeAtual() . "',
                            '" . $estoque->getQuantidadeReservada() . "'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();
            $query = "SELECT * FROM estoque";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){
                    $estoque = new EstoqueModel();
                    $estoque->setCodigo($dados['codigo']);
                    $estoque->setCodigoProduto($dados['codigo_produto']);
                    $estoque->setMaximo($dados['maximo']);
                    $estoque->setMinimo($dados['minimo']);
                    $estoque->setQuantidadeAtual($dados['quantidade_atual']);
                    $estoque->setQuantidadeReservada($dados['quantidade_reservada']);
                    array_push($lista, $estoque);
                }
                return $lista;
            }
        }
        public function editar($estoque){
            $banco = new Banco();
            $query = "  UPDATE 
                            estoque 
                        SET
                            codigo_produto = '" . $estoque->getCodigoProduto() . "',
                            maximo = '" . $estoque->getMaximo() . "',
                            minimo = '" . $estoque->getMinimo() . "',
                            quantidade_atual = '" . $estoque->getQuantidadeAtual() . "',
                            quantidade_reservada = '" . $estoque->getQuantidadeReservada() . "'
                        WHERE
                            codigo = " . $estoque->getCodigo() . " ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM estoque WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $estoque = new EstoqueModel();
                $estoque->setCodigo($dados['codigo']);
                $estoque->setCodigoProduto($dados['codigo_produto']);
                $estoque->setMaximo($dados['maximo']);
                $estoque->setMinimo($dados['minimo']);
                $estoque->setQuantidadeAtual($dados['quantidade_atual']);
                $estoque->setQuantidadeReservada($dados['quantidade_reservada']);
                return $estoque;
            }
            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM estoque WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }

    }
