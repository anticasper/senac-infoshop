
<?php
/* =============================================
    CodigoMarca: produtoDao.php
    Autor: Diovanni de Souza
    Data Criação: 11/03/2020
    Descrição: Dao das Produtos
    Ultima Mudança: 11/03/2020

    private $codigo;
        private $codigoMarca;
        private $descricao;
        private $ean;
        private $nome;
        private $sku;
============================================= */
    include_once "classes/models/produto.php";
    include_once "includes/banco.php";
    class ProdutoDao{
        public function incluir($produto){
            $banco = new Banco();
            $query = "INSERT INTO 
                        produto 
                        (
                            codigo,
                            codigo_marca,
                            descricao,
                            ean,
                            nome,
                            sku
                        ) VALUES (
                            NULL,
                            '" . $produto->getCodigoProduto() . "',
                            '" . $produto->getDescricao() . "',
                            '" . $produto->getEan() . "',
                            '" . $produto->getNome() . "',
                            '" . $produto->getSku() . "'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();
            $query = "SELECT * FROM produto";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){
                    $produto = new ProdutoModel();
                    $produto->setCodigo($dados['codigo']);
                    $produto->setCodigoMarca($dados['codigo_marca']);
                    $produto->setDescricao($dados['descricao']);
                    $produto->setEan($dados['ean']);
                    $produto->setNome($dados['nome']);
                    $produto->setSku($dados['sku']);
                    array_push($lista, $produto);
                }
                return $lista;
            }
        }
        public function editar($produto){
            $banco = new Banco();
            $query = "  UPDATE 
                            produto 
                        SET
                            codigo_marca = '" . $produto->getCodigoProduto() . "',
                            descricao = '" . $produto->getDescricao() . "',
                            ean = '" . $produto->getEan() . "',
                            nome = '" . $produto->getNome() . "',
                            sku = '" . $produto->getSku() . "'
                        WHERE
                            codigo = " . $produto->getCodigo() . " ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM produto WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $produto = new ProdutoModel();
                $produto->setCodigo($dados['codigo']);
                $produto->setCodigoMarca($dados['codigo_marca']);
                $produto->setDescricao($dados['descricao']);
                $produto->setEan($dados['ean']);
                $produto->setNome($dados['nome']);
                $produto->setSku($dados['sku']);
                return $produto;
            }
            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM produto WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }

    }
