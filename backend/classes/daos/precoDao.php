
<?php
/* =============================================
    CodigoProduto: precoDao.php
    Autor: Diovanni de Souza
    Data Criação: 11/03/2020
    Descrição: Dao das Precos
    Ultima Mudança: 11/03/2020
    private $codigo;
    private $codigoProduto;
    private $dataEntrada;
    private $dataLimite;
    private $valor;
============================================= */
    include_once "classes/models/preco.php";
    include_once "includes/banco.php";
    class PrecoDao{
        public function incluir($preco){
            $banco = new Banco();
            $query = "INSERT INTO 
                        preco 
                        (
                            codigo,
                            codigo_produto,
                            data_entrada,
                            data_limite,
                            valor
                        ) VALUES (
                            NULL,
                            '" . $preco->getCodigoProduto() . "',
                            '" . $preco->getDataEntrada() . "',
                            '" . $preco->getDataLimite() . "',
                            '" . $preco->getValor() . "'
                        )";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function listar(){
            $banco = new Banco();
            $query = "SELECT * FROM preco";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $lista = array();
                while($dados = mysqli_fetch_array($resultado)){
                    $preco = new PrecoModel();
                    $preco->setCodigo($dados['codigo']);
                    $preco->setCodigoProduto($dados['codigo_produto']);
                    $preco->setDataEntrada($dados['data_entrada']);
                    $preco->setDataLimite($dados['data_limite']);
                    $preco->setValor($dados['valor']);
                    array_push($lista, $preco);
                }
                return $lista;
            }
        }
        public function editar($preco){
            $banco = new Banco();
            $query = "  UPDATE 
                            preco 
                        SET
                            codigo_produto = '" . $preco->getCodigoProduto() . "',
                            data_entrada = '" . $preco->getDataEntrada() . "',
                            data_limite = '" . $preco->getDataLimite() . "',
                            valor = '" . $preco->getValor() . "'
                        WHERE
                            codigo = " . $preco->getCodigo() . " ";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }
        public function obter($codigo){
            $banco = new Banco();
            $query = "SELECT * FROM preco WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            if($resultado){
                $dados = mysqli_fetch_array($resultado);
                $preco = new PrecoModel();
                $preco->setCodigo($dados['codigo']);
                $preco->setCodigoProduto($dados['codigo_produto']);
                $preco->setDataEntrada($dados['data_entrada']);
                $preco->setDataLimite($dados['data_limite']);
                $preco->setValor($dados['valor']);
                return $preco;
            }
            return $resultado;
        }
        public function deletar($codigo){
            $banco = new Banco();
            $query = "DELETE FROM preco WHERE codigo = $codigo";
            $resultado = $banco->getDb()->query($query);
            return $resultado;
        }

    }
