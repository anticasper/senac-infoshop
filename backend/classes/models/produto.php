<?php
/* =============================================
    Nome: produto.php
    Autor: Diovanni de Souza
    Data Criação: 06/03/2020
    Descrição: Modelo de Produtos
    Ultima Mudança: 06/03/2020
============================================= */
    class ProdutoModel {
        private $codigo;
        private $codigoMarca;
        private $descricao;
        private $ean;
        private $nome;
        private $sku;
        
        public function getCodigo(){
            return $this->codigo;
        }

        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }

        public function getCodigoMarca(){
            return $this->codigoMarca;
        }

        public function setCodigoMarca($codigoMarca){
            $this->codigoMarca = $codigoMarca;
        }

        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function getDescricao(){
            return $this->descricao;
        }

        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }

        public function setEan($ean){
            $this->ean = $ean;
        }
        public function getEan(){
            return $this->ean;
        }

        public function getSku(){
            return $this->sku;
        }

        public function setSku($sku){
            $this->sku = $sku;
        }


    }