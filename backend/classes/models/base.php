<?php
/* =============================================
    Nome: base.php
    Autor: Diovanni de Souza
    Data Criação: 06/03/2020
    Descrição: Modelo de Classe Models
    Ultima Mudança: 06/03/2020
============================================= */
    class BaseModel {
        private $codigo;
        
        public function getCodigo(){
            return $this->codigo;
        }

        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }
    }