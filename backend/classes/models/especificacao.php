<?php
/* =============================================
    Nome: especificao.php
    Autor: Diovanni de Souza
    Data Criação: 06/03/2020
    Descrição: Classe Especificação
    Ultima Mudança: 06/03/2020
============================================= */
    class EspecificacaoModel {
        private $codigo;
        private $codigoProduto;
        private $nome;
        private $descricao;
        
        public function getCodigo(){
            return $this->codigo;
        }

        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }

        public function getCodigoProduto(){
            return $this->codigoProduto;
        }

        public function setCodigoProduto($codigoProduto){
            $this->codigoProduto = $codigoProduto;
        }

        public function getNome(){
            return $this->nome;
        }

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function getDescricao(){
            return $this->descricao;
        }

        public function setDescricao($descricao){
            $this->descricao = $descricao;
        }
        
    }