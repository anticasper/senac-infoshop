<?php
/* =============================================
    Nome: estoque.php
    Autor: Diovanni de Souza
    Data Criação: 06/03/2020
    Descrição: Classe Estoque
    Ultima Mudança: 06/03/2020
============================================= */
    class EstoqueModel {
        private $codigo;
        private $codigoProduto;
        private $maximo;
        private $minimo;
        private $quantidadeAtual;
        private $quantidadeReservada;

        public function getCodigo(){
            return $this->codigo;
        }

        public function setCodigo($codigo){
            $this->codigo = $codigo;
        }

        public function getCodigoProduto(){
            return $this->codigoProduto;
        }

        public function setCodigoProduto($codigoProduto){
            $this->codigoProduto = $codigoProduto;
        }

        public function getMaximo(){
            return $this->maximo;
        }

        public function setMaximo($maximo){
            $this->maximo = $maximo;
        }

        public function getMinimo(){
            return $this->minimo;
        }

        public function setMinimo($minimo){
            $this->minimo = $minimo;
        }

        public function getQuantidadeAtual(){
            return $this->quantidadeAtual;
        }

        public function setQuantidadeAtual($quantidadeAtual){
            $this->quantidadeAtual = $quantidadeAtual;
        }

        public function getQuantidadeReservada(){
            return $this->quantidadeReservada;
        }

        public function setQuantidadeReservada($quantidadeReservada){
            $this->quantidadeReservada = $quantidadeReservada;
        }
    }