<?php
/* =============================================
    Nome: index.php
    Autor: Diovanni de Souza
    Data Criação: 06/03/2020
    Descrição: Página inicial do sistema
    Ultima Mudança: 06/03/2020
============================================= */
// Includes
include_once "includes/controle.php";
// Tratativas
$objetoControle = new Controle();
// Controle
$objetoControle->defineRota($_GET);
$objetoControle->executarRota($_POST);
// Log

?>
