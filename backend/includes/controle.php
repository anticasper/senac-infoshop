<?php
/* =============================================
    Nome: controle.php
    Autor: Diovanni de Souza
    Data Criação: 06/03/2020
    Descrição: Controlador do Aplicativo
    Ultima Mudança: 06/03/2020
============================================= */
    include_once "classes/models/marca.php";
    include_once "classes/models/produto.php";
    include_once "classes/daos/marcaDao.php";
    include_once "classes/daos/produtoDao.php";
    class Controle {
        private $rota;
    
        public function imprimeRota(){
            return $this->rota;
        }

        public function defineRota($rota){
            $this->rota = $rota;
        }

        public function executarRota($data){
            if(isset($this->rota['acao'])){
               switch ($this->rota['acao']) {
                   case 'produto':
                        $this->gerenciaProduto($data);
                    break;
                    case 'marca':
                        $this->gerenciaMarca($data);
                    break;
                    case 'preco':
                        $this->gerenciaPreco($data);
                    break;
                    case 'estoque':
                        $this->gerenciaEstoque($data);
                    break;
                    case 'especificacao':
                        $this->gerenciaEspecificao($data);
                       break;
                    case 'teste' :                    
                        echo "Teste Ok";
                    break;
                   default:
                       break;
               }
            }
        
        }
        /* ========================================================
            Função responsável por gerenciar Produtos
        ======================================================== */
        public function gerenciaProduto($data){            
            if(isset($this->rota['metodo'])){
                $metodo = $this->rota['metodo'];
                $objProdutoDao = new ProdutoDao();
                // Executar metodo
                switch ($metodo) {
                    case 'incluir':
                        $objProduto = new ProdutoModel();
                        $objProduto->setCodigo($data['codigo']);
                        $objProduto->setNome($data['nome']);
                        $objProduto->setEan($data['ean']);
                        $objProduto->setSku($data['sku']);
                        $objProduto->setDescricao($data['descricao']);
                        $objProduto->setCodigoMarca($data['codigoMarca']);
                        $objProdutoDao->incluir($objProduto);
                        echo "Inserido com Sucesso";
                        break;
                    case 'editar':
                        $objProduto = new ProdutoModel();
                        $objProduto->setCodigo($data['codigo']);
                        $objProduto->setNome($data['nome']);
                        $objProduto->setEan($data['ean']);
                        $objProduto->setSku($data['sku']);
                        $objProduto->setDescricao($data['descricao']);
                        $objProduto->setCodigoMarca($data['codigoMarca']);
                        $objProdutoDao->editar($objProduto);
                        echo "Alterado com Sucesso";
                        break;
                    case 'listar':
                        $retorno = $objProdutoDao->listar();
                        //echo json_encode($retorno);
                        var_dump($retorno);
                        break;
                    case 'obter':
                        $retorno = $objProdutoDao->obter($data['codigo']);
                        var_dump($retorno);
                        break;
                    case 'deletar':
                        $retorno = $objProdutoDao->deletar($data['codigo']);
                        var_dump($retorno);
                        break;
                    default:
                        echo "Erro: $metodo inexistente";
                        break;
                }
            }else{
                echo "Erro: Método não encontrado";
            }
        }
        /* ========================================================
            Função responsável por gerenciar Produtos
        ======================================================== */
        public function gerenciaMarca($data){            
            if(isset($this->rota['metodo'])){
                $metodo = $this->rota['metodo'];
                $objMarcaDao = new MarcaDao();
                // Executar metodo
                switch ($metodo) {
                    case 'incluir':
                        $objMarca = new MarcaModel();
                        $objMarca->setCodigo($data['codigo']);
                        $objMarca->setNome($data['nome']);
                        $objMarca->setDescricao($data['descricao']);
                        $objMarca->setLogo($data['codigoLogo']);
                        $objMarcaDao->incluir($objMarca);
                        echo "Inserido com Sucesso";
                        break;
                    case 'editar':
                        $objMarca = new MarcaModel();
                        $objMarca->setCodigo($data['codigo']);
                        $objMarca->setNome($data['nome']);
                        $objMarca->setDescricao($data['descricao']);
                        $objMarca->setLogo($data['codigoLogo']);
                        $objMarcaDao->editar($objMarca);
                        echo "Alterado com Sucesso";
                        break;
                    case 'listar':
                        $retorno = $objMarcaDao->listar();
                        //echo json_encode($retorno);
                        var_dump($retorno);
                        break;
                    case 'obter':
                        $retorno = $objMarcaDao->obter($data['codigo']);
                        var_dump($retorno);
                        break;
                    case 'deletar':
                        $retorno = $objMarcaDao->deletar($data['codigo']);
                        var_dump($retorno);
                        break;
                    default:
                        echo "Erro: $metodo inexistente";
                        break;
                }
            }else{
                echo "Erro: Método não encontrado";
            }
        }
        /* ========================================================
            Função responsável por gerenciar Produtos
        ======================================================== */
        public function gerenciaPreco($data){            
            if(isset($this->rota['metodo'])){
                $metodo = $this->rota['metodo'];
                $objPrecoDao = new PrecoDao();
                // Executar metodo
                switch ($metodo) {
                    case 'incluir':
                        $objPreco = new PrecoModel();
                        $objPreco->setCodigo($data['codigo']);
                        $objPreco->setCodigoProduto($data['codigoProduto']);
                        $objPreco->setDataEntrada($data['dataEntrada']);
                        $objPreco->setDataLimite($data['dataLimite']);
                        $objPreco->setValor($data['valor']);
                        $objPrecoDao->incluir($objPreco);
                        echo "Inserido com Sucesso";
                        break;
                    case 'editar':
                        $objPreco = new PrecoModel();
                        $objPreco->setCodigo($data['codigo']);
                        $objPreco->setCodigoProduto($data['codigoProduto']);
                        $objPreco->setDataEntrada($data['dataEntrada']);
                        $objPreco->setDataLimite($data['dataLimite']);
                        $objPreco->setValor($data['valor']);
                        $objPrecoDao->editar($objPreco);
                        echo "Alterado com Sucesso";
                        break;
                    case 'listar':
                        $retorno = $objPrecoDao->listar();
                        //echo json_encode($retorno);
                        var_dump($retorno);
                        break;
                    case 'obter':
                        $retorno = $objPrecoDao->obter($data['codigo']);
                        var_dump($retorno);
                        break;
                    case 'deletar':
                        $retorno = $objPrecoDao->deletar($data['codigo']);
                        var_dump($retorno);
                        break;
                    default:
                        echo "Erro: $metodo inexistente";
                        break;
                }
            }else{
                echo "Erro: Método não encontrado";
            }
        }
        /* ========================================================
            Função responsável por gerenciar Produtos
        ======================================================== */
        public function gerenciaEstoque($data){            
            if(isset($this->rota['metodo'])){
                $metodo = $this->rota['metodo'];
                $objEstoqueDao = new EstoqueDao();
                // Executar metodo
                switch ($metodo) {
                    case 'incluir':
                        $objEstoque = new EstoqueModel();
                        $objEstoque->setCodigo($data['codigo']);
                        $objEstoque->setCodigoProduto($data['codigoProduto']);
                        $objEstoque->setMaximo($data['maximo']);
                        $objEstoque->setMinimo($data['minimo']);
                        $objEstoque->setQuantidadeAtual($data['quantidadeAtual']);
                        $objEstoque->setQuantidadeReservada($data['quantidadeReservada']);
                        $objEstoqueDao->incluir($objEstoque);
                        echo "Inserido com Sucesso";
                        break;
                    case 'editar':
                        $objEstoque = new EstoqueModel();
                        $objEstoque->setCodigo($data['codigo']);
                        $objEstoque->setCodigoProduto($data['codigoProduto']);
                        $objEstoque->setMaximo($data['maximo']);
                        $objEstoque->setMinimo($data['minimo']);
                        $objEstoque->setQuantidadeAtual($data['quantidadeAtual']);
                        $objEstoque->setQuantidadeReservada($data['quantidadeReservada']);
                        $objEstoqueDao->editar($objEstoque);
                        echo "Alterado com Sucesso";
                        break;
                    case 'listar':
                        $retorno = $objEstoqueDao->listar();
                        //echo json_encode($retorno);
                        var_dump($retorno);
                        break;
                    case 'obter':
                        $retorno = $objEstoqueDao->obter($data['codigo']);
                        var_dump($retorno);
                        break;
                    case 'deletar':
                        $retorno = $objEstoqueDao->deletar($data['codigo']);
                        var_dump($retorno);
                        break;
                    default:
                        echo "Erro: $metodo inexistente";
                        break;
                }
            }else{
                echo "Erro: Método não encontrado";
            }
        }
        /* ========================================================
            Função responsável por gerenciar Produtos
        ======================================================== */
        public function gerenciaEspecificao($data){            
            if(isset($this->rota['metodo'])){
                $metodo = $this->rota['metodo'];
                $objEspecificacaoDao = new EspecificacaoDao();
                // Executar metodo
                switch ($metodo) {
                    case 'incluir':
                        $objEspecificacao = new EspecificacaoModel();
                        $objEspecificacao->setCodigo($data['codigo']);
                        $objEspecificacao->setNome($data['nome']);
                        $objEspecificacao->setDescricao($data['descricao']);
                        $objEspecificacao->setCodigoProduto($data['codigoProduto']);
                        $objEspecificacaoDao->incluir($objEspecificacao);
                        echo "Inserido com Sucesso";
                        break;
                    case 'editar':
                        $objEspecificacao = new EspecificacaoModel();
                        $objEspecificacao->setCodigo($data['codigo']);
                        $objEspecificacao->setNome($data['nome']);
                        $objEspecificacao->setDescricao($data['descricao']);
                        $objEspecificacao->setCodigoProduto($data['codigoProduto']);
                        $objEspecificacaoDao->editar($objEspecificacao);
                        echo "Alterado com Sucesso";
                        break;
                    case 'listar':
                        $retorno = $objEspecificacaoDao->listar();
                        //echo json_encode($retorno);
                        var_dump($retorno);
                        break;
                    case 'obter':
                        $retorno = $objEspecificacaoDao->obter($data['codigo']);
                        var_dump($retorno);
                        break;
                    case 'deletar':
                        $retorno = $objEspecificacaoDao->deletar($data['codigo']);
                        var_dump($retorno);
                        break;
                    default:
                        echo "Erro: $metodo inexistente";
                        break;
                }
            }else{
                echo "Erro: Método não encontrado";
            }
        }
    }
?>
