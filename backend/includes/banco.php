<?php
/* =============================================
    Nome: banco.php
    Autor: Diovanni de Souza
    Data Criação: 09/03/2020
    Descrição: Interface de Conexão com o Banco
    Ultima Mudança: 09/03/2020

    mysqli
============================================= */
    class Banco {

        private $db;
    
        function Banco(){
            $servidor   = "localhost";
            $usuario    = "root";
            $senha      = "";
            $banco      = "loja_informatica";
            $this->setDb(new MySQLi($servidor, $usuario, $senha, $banco));
        }

        public function getDb(){
           return $this->db;
        }

        public function setDb($db){
            $this->db = $db;
        }
    

    }